package com.adaptavist

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.user.util.UserManager
import com.onresolve.scriptrunner.runner.rest.common.CustomEndpointDelegate
import groovy.json.JsonBuilder
import groovy.transform.BaseScript

import javax.ws.rs.core.MultivaluedMap
import javax.ws.rs.core.Response

@BaseScript CustomEndpointDelegate delegate

do2(httpMethod: "GET", groups: ["jira-administrators"]) { MultivaluedMap queryParams, String body ->
    return Response.ok(new JsonBuilder([abc: "hello world!!!", def:"some more values."]).toString()).build();
}

showme(httpMethod: "GET", groups: ["jira-administrators"]) { MultivaluedMap queryParams, String body ->
    def d = []
    d << "adaptavist.com"
    d << "gmail.com"


    return Response.ok(new JsonBuilder([domains: d]).toString()).build();
}

domains ( httpMethod :"GET", groups: ["jira-administrators"]) {
    UserManager userManager = ComponentAccessor.getUserManager() as UserManager
    def users = userManager.getAllUsers()
    def domains = []

    users.each {user ->
        String e = user.emailAddress
        if (e?.contains('@')) {
            domains << e?.split('@')[1]
        }
    }
    domains.unique()

    Response.ok( domains ).build()


}